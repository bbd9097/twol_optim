from setuptools import setup

setup(
    name='2L-VKOGA',
    version='0.1.2',    
    description='Python implementation of the two-layered (2L) Vectorial Kernel Orthogonal Greedy Algorithm',
    url='https://gitlab.mathematik.uni-stuttgart.de/pub/ians-anm/2l-vkoga',
    author='Tizian Wenzel',
    author_email='tizian.wenzel@mathematik.uni-stuttgart.de',
    license='GNU v3.0',
    packages=['vkoga_2L'],
    install_requires=[],
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',  
        'Operating System :: POSIX :: Linux',        
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
)

