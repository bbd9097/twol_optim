# 2L-VKOGA

Python implementation of the 2L-VKOGA algorithm without specified requirements.


## Installation

pip install git+https://gitlab.rrz.uni-hamburg.de/bbd9097/twol_optim/

## Usage

Have a look into the [demo file](https://gitlab.mathematik.uni-stuttgart.de/pub/ians-anm/2l-vkoga/-/blob/main/example_files/01_testfile.py).

## How to cite:
If you use this code in your work for scalar-valued output data, please cite the paper

> T. Wenzel, F. Marchetti, and E. Perracchione. Data-driven kernel designs for
optimized greedy schemes: A machine learning perspective. SIAM Journal
on Scientific Computing, 46(1):C101–C126, 2024


```bibtex:
@article{wenzel2024data,
	author = {Wenzel, Tizian and Marchetti, Francesco and Perracchione, Emma},
	title = {Data-Driven Kernel Designs for Optimized Greedy Schemes: A Machine Learning Perspective},
	journal = {SIAM Journal on Scientific Computing},
	volume = {46},
	number = {1},
	pages = {C101-C126},
	year = {2024},
	doi = {10.1137/23M1551201},
	URL = {https://epubs.siam.org/doi/abs/10.1137/23M1551201},
	eprint = {https://epubs.siam.org/doi/pdf/10.1137/23M1551201}
}
```

If you use this code in your work for vectorial-valued output data, please cite the paper

> T. Wenzel, B. Haasdonk, H. Kleikamp, M. Ohlberger, and F. Schindler. Application 
of Deep Kernel Models for Certified and Adaptive RB-ML-ROM Surrogate Modeling. 
In I. Lirkov and S. Margenov, editors, Large-Scale Scientific
Computations, pages 117–125, Cham, 2024. Springer Nature Switzerland


```bibtex:
@InProceedings{wenzel2024application,
author="Wenzel, Tizian and Haasdonk, Bernard and Kleikamp, Hendrik and Ohlberger, Mario and Schindler, Felix",
editor="Lirkov, Ivan and Margenov, Svetozar",
title="{A}pplication of {D}eep {K}ernel {M}odels for {C}ertified and {A}daptive {RB}-{ML}-{ROM} {S}urrogate {M}odeling",
booktitle="Large-Scale Scientific Computations",
year="2024",
publisher="Springer Nature Switzerland",
address="Cham",
pages="117--125",
doi="doi.org/10.1007/978-3-031-56208-2_11",
}
```


For further details on the VKOGA algorithm, please refer to [here](https://github.com/GabrieleSantin/VKOGA).

