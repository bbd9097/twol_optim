# Example to show the use of validation set tracking

import numpy as np
from matplotlib import pyplot as plt
from vkoga_2L import kernels, tkernels
from vkoga_2L.vkoga_2L import VKOGA_2L

np.random.seed(1)


# Create some 2D data, whereby the target values are invariant in one direction
dim = 3
X_train = np.random.rand(500, dim)
y_train = X_train[:, [0]]

X_val = np.random.rand(100, dim)
y_val = X_val[:, [0]]


# Run VKOGA
kernel = kernels.Matern(k=1)
kernel_t = tkernels.Matern(k=1)

model_1L = VKOGA_2L(kernel=kernel, greedy_type='f_greedy')
_ = model_1L.fit(X_train, y_train, X_val=X_val, y_val=y_val, maxIter=50)

model_2L = VKOGA_2L(kernel=[kernel, kernel_t], greedy_type='f_greedy', flag_2L_optimization=True)
_ = model_2L.fit(X_train, y_train, X_val=X_val, y_val=y_val, maxIter=50)


# Get ready for some plot
fig = plt.figure(2)
fig.clf()
plt.plot(model_1L.train_hist['f'])
plt.plot(model_2L.train_hist['f'])
plt.legend(['1L, f max', '2L, f max'])
plt.xlabel('training iteration')
plt.xscale('log')
plt.yscale('log')
plt.draw()








